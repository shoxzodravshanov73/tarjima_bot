package specialClasses;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Dictionary {

	@lombok.Setter
	@SerializedName("head")
	private Head head;

	@SerializedName("def")
	private List<DefItem> def;

	public Head getHead(){
		return head;
	}

	public void setDef(List<DefItem> def){
		this.def = def;
	}

	public List<DefItem> getDef(){
		return def;
	}

	@Override
 	public String toString(){
		return 
			"Translater{" + 
			"head = '" + head + '\'' + 
			",def = '" + def + '\'' + 
			"}";
		}
}