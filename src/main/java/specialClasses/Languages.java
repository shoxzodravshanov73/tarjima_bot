package specialClasses;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class Languages {

    static List<String>language=new ArrayList<>();
    public static List<String> getLanguage() {
        Gson gson = new Gson();
        URL url;
        {
            try {
                url = new URL("https://dictionary.yandex.net/api/v1/dicservice.json/getLangs?key=dict.1.1.20210413T060941Z.7becc0d8eb6ec57e.4865a924d20b309b61f0cf6b88f835482943a23c");
                URLConnection urlConnection = url.openConnection();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                Type type = new TypeToken<List<String>>() {

                }.getType();
                List<String> strings = gson.fromJson(bufferedReader, type);
                language.addAll(strings);

            } catch (IOException e) {
                e.printStackTrace();
            }
            return language;

        }
    }

}
