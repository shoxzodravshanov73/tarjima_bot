package model;

import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

public class TarjimaBot extends TelegramLongPollingBot {


    @Override
    public String getBotUsername() {
        return "yandex_lugat_uz_bot";
    }

    @Override
    public String getBotToken() {
        return "1735254560:AAFq8455Kt79IQGv3sUwpkxewNmyvalOQ9I";
    }
    @Override
    public void onUpdateReceived(Update update) {

        if (update.hasMessage()){
            String text = update.getMessage().getText();
            if (text.equals("/start")){
                try {
                    execute(Service.start(update));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            }else{
                TgUser user=Service.userniTopibQoshish(update);
                if (user.getBotState().equals(BotState.ENTER_WORD)){
                    try {
                        execute(Service.tarjimaQilish(update));
                        execute(Service.start(update));
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }
            }

        }else if (update.getCallbackQuery()!=null){
            String data=update.getCallbackQuery().getData();
            if (data.contains("-")){
                try {
                    execute(Service.tanlanganTilniBilish(update));
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
