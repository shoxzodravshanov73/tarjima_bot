package model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TgUser {

    private long chatId;
    private BotState botState;
    private String fromLanguge;
    private String toLanguge;
}
