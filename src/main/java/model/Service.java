package model;

import com.google.gson.Gson;
import javafx.scene.transform.Translate;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.User;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import specialClasses.Dictionary;
import specialClasses.Languages;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class Service {

    static List<TgUser> users=new ArrayList<>();

    public static SendMessage start(Update update){
        SendMessage sendMessage=new SendMessage();

        Message message=update.getMessage();
        sendMessage.setChatId(message.getChatId());
        User user= message.getFrom();


        sendMessage.setChatId(update.getMessage().getChatId());
        sendMessage.setText("Assalomu aleykum  "+user.getFirstName()+"  bizning tarjima bot ga xo'sh kelibsiz!!! \n" +
               "Tilni tanlang" );

        InlineKeyboardMarkup inlineKeyboardMarkup=new InlineKeyboardMarkup();

        List<List<InlineKeyboardButton>> inlineRows=new ArrayList<>();
        List<InlineKeyboardButton> inlineRow1=new ArrayList<>();
        InlineKeyboardButton button1=new InlineKeyboardButton("Eng => Rus");
        button1.setCallbackData("en-ru");
        InlineKeyboardButton button2=new InlineKeyboardButton("Rus => Eng");
        button2.setCallbackData("ru-en");
        inlineRow1.add(button1);
        inlineRow1.add(button2);

        List<InlineKeyboardButton> inlineRow2=new ArrayList<>();
        InlineKeyboardButton button3=new InlineKeyboardButton("Turk => Rus");
        button3.setCallbackData("tr-ru");
        InlineKeyboardButton button4=new InlineKeyboardButton("Rus => Turk");
        button4.setCallbackData("ru-tr");
        inlineRow2.add(button3);
        inlineRow2.add(button4);

        List<InlineKeyboardButton> inlineRow3=new ArrayList<>();
        InlineKeyboardButton button5=new InlineKeyboardButton("Turk => Eng");
        button5.setCallbackData("tr-en");
        InlineKeyboardButton button6=new InlineKeyboardButton("Eng => Turk");
        button6.setCallbackData("en-tr");
        inlineRow3.add(button5);
        inlineRow3.add(button6);

        inlineRows.add(inlineRow1);
        inlineRows.add(inlineRow2);
        inlineRows.add(inlineRow3);

        inlineKeyboardMarkup.setKeyboard(inlineRows);
        sendMessage.setReplyMarkup(inlineKeyboardMarkup);

        return  sendMessage;


    }
    public static TgUser userniTopibQoshish(Update update){
        long chatId=update.getCallbackQuery()!=null?
                update.getCallbackQuery().getMessage().getChatId():
                update.getMessage().getChatId();
        for (TgUser user : users) {
            if (user.getChatId()==chatId){
                return user;
            }
        }
        TgUser tgUser=new TgUser(chatId,BotState.CHOOSE_LANGUAGE,null,null);
        users.add(tgUser);
        return tgUser;
    }
    public static void  userniYangilash(TgUser tgUser){
        for (TgUser user : users) {
            if (tgUser.getChatId()==user.getChatId()){
                tgUser=user;
                break;
            }
        }
    }
    public static SendMessage tanlanganTilniBilish(Update update){
        SendMessage sendMessage=new SendMessage();

        long chatId=update.getCallbackQuery().getMessage().getChatId();
        String kelganCallbecData=update.getCallbackQuery().getData();
        String fromLanguage=kelganCallbecData.substring(0,kelganCallbecData.indexOf("-"));
        String toLanguage=kelganCallbecData.substring(kelganCallbecData.indexOf("-")+1);
        TgUser tgUser=userniTopibQoshish(update);
        boolean bool=false;

        for (String s : Languages.getLanguage()) {
            if (s.equals(kelganCallbecData)){
                bool=true;
                break;
            }
        }
        if (bool){
            tgUser.setFromLanguge(fromLanguage);
            tgUser.setToLanguge(toLanguage);
            tgUser.setBotState(BotState.ENTER_WORD);
            userniYangilash(tgUser);

            if (fromLanguage.equals("en")){
                sendMessage.setText("Enter word");
            }else if (fromLanguage.equals("ru")){
                sendMessage.setText("Введите слово");
            }else if (fromLanguage.equals("tr")){
                sendMessage.setText("Kelime gir");
            }
        }else{
            sendMessage.setText("Hozircha bunday til mavjud emas : ");
        }
        sendMessage.setChatId(chatId);
        return  sendMessage;




    }
    public static SendMessage tarjimaQilish(Update update){
        Gson gson=new Gson();
        SendMessage sendMessage=new SendMessage();
        long chatId=update.getMessage().getChatId();
        String qidiruv=update.getMessage().getText();
        TgUser tgUser=userniTopibQoshish(update);

        String fromLanguage= tgUser.getFromLanguge();
        String toLanguage= tgUser.getToLanguge();


        URL url= null;
        try {
            url = new URL("https://dictionary.yandex.net/api/v1/dicservice.json/lookup?key=dict.1.1.20210413T060941Z.7becc0d8eb6ec57e.4865a924d20b309b61f0cf6b88f835482943a23c&lang="+fromLanguage+"-"+toLanguage+"&text="+qidiruv);
            URLConnection urlConnection= url.openConnection();
            BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            Dictionary dictionaries=gson.fromJson(bufferedReader,Dictionary.class);

            if (dictionaries.getDef().size()!=0) {
                sendMessage.setText(dictionaries.getDef().get(0).getTr().get(0).getText());

            }else{
                sendMessage.setText("Ushbu so'z topilmadi");
            }




        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

sendMessage.setChatId(chatId);
        return sendMessage;
    }
}
